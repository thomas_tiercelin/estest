var _ = require('underscore');
var moment = require('moment');
var Promise = require('promise');
var Document = require('./Document.js');

function Entry(obj) {
  var _this = this;
  _this.base = Document;
  _this.base(obj);
}

Entry.prototype = new Document;
Entry.prototype.type = "Entry";

Entry.get = function(id) {
  return new Promise(function(resolve, reject) {
    Document.get(id, Entry.prototype.index, Entry.prototype.type).then(function(ent){
      resolve(new Entry(ent));
    });
  });
};

Entry.list = function(query, size) {
  return Document.list(Entry.prototype.index, Entry.prototype.type, query, size);
};

Entry.generate = function(amount, currency, label) {
  return new Entry({
    amount:amount,
    currency:currency,
    label:label
  });
};

Entry.Currency = {
  EUR: 'EUR',
  USD: 'USD'
}

module.exports = Entry;
