var _ = require('underscore');
var moment = require('moment');
var Promise = require('promise');

function Document(obj) {
  var _this = this;
  if(obj) {
    _this.load(obj);
  }
}

Document.prototype.index = "main-index-2";
Document.prototype.versionIndex = "version-index";
Document.prototype.type = "Document";

Document.get = function(id, index, type) {
  return Document.client.get({
    index: index,
    type: type,
    id: id
  });
};

Document.list = function(index, type, query, size) {
  return Document.client.search({
    index: index,
    type: type,
    size: size,
    sort: 'createdAt:desc',
    q: query
  });
};

Document.prototype.load = function(obj) {
  var _this = this;
  for(var k in obj) {
      _this[k] = obj[k];
  }
};

Document.prototype.save = function(doc) {
  var _this = this;
  if(!_this._id) {
    return _this._create();
  }
  return _this._exists().then(function(exists){
    if(exists) {
      doc = doc || _this._source;
      return _this._update(doc);
    }
    else {
      return _this._create();
    }
  });
};

Document.prototype._exists = function() {
  var _this = this;
  return Document.client.exists({
    index: _this.index,
    type: _this.type,
    id: _this._id
  });
};

Document.prototype._prepare = function() {
  return _.clone(this.doc);
};

Document.prototype.listVersions = function(query, size) {
  var _this = this;
  console.log('list versions', _this.type.toLowerCase());
  return Document.client.search({
    index: Document.prototype.versionIndex+'-'+_this.type.toLowerCase(),
    type: _this._id,
    size: size,
    sort: 'createdAt:asc',
    q: query
  });
};

Document.prototype.saveVersion = function(result) {
  var _this = this;

  var version = _.clone(_this);
  version.master = {
    id: result._id,
    version: result._version,
    type: result._type,
    index: result._index
  };
  delete version.base;
  delete version._id;
  delete version._version;
  delete version._index;
  delete version._type;

  if(version._source) {
    for(var k in version._source) {
      version[k] = version._source[k];
    }
  }

  delete version._source;

  version.createdAt = new Date();

  Document.client.create({
    index: Document.prototype.versionIndex+'-'+_this.type.toLowerCase(),
    type: result._id,
    body: version
  });

  return new Promise(function(resolve, reject) {
    resolve(result);
  });
};

Document.prototype._create = function() {
  var _this = this;
  _this.createdAt = new Date();
  _this.updatedAt = new Date();
  return Document.client.create({
    index: _this.index,
    type: _this.type,
    id: _this._id,
    body: this
  }).then(function(result){
    return _this.saveVersion(result);
  });
};

Document.prototype._update = function(doc) {
  var _this = this;
  doc = doc || _this._source;
  doc.updatedAt = new Date();
  return Document.client.update({
    index: _this.index,
    type: _this.type,
    id: _this._id,
    body: {
      doc: doc
    }
  }).then(function(result){
    return _this.saveVersion(result);
  });
};

Document.prototype.toExt = function() {
  var _this = this;
  var obj = _.clone(_this._source);
  obj.id = _this._id;
  obj.version = _this._version;
  obj.type = _this._type;
  return obj;
};

Document.listToExt = function(list) {
  var l = [];
  _.each(list, function(element){
    l.push(element.toExt());
  });
  return l;
};

module.exports = Document;
