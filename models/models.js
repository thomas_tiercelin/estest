var Document = require('./Document.js');
var Entry = require('./Entry.js');

module.exports = function(client) {
  Document.client = client;

  var exports = {};
  exports.Entry = Entry;
  return exports;
}
