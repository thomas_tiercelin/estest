console.log('init');

var elasticsearch = require('elasticsearch');
var client = new elasticsearch.Client({
  host: 'localhost:9200',
  log: 'trace'
});

var models = require('./models/models.js')(client);
var Entry = models.Entry;
var Document = models.Document;

var express = require('express');
var _ = require('underscore');
var moment = require('moment');
var app = express();

var router = express.Router();

var publicDir = __dirname + '/public';

app.use(express.static(publicDir));

router.use(function(req, res, next) {
	next();
});

router.get('/', function(req, res) {
  _.times(100, function(){
      Entry.generate(100, Entry.Currency.EUR, 'super label').save();
  });
	res.json({ message: 'hooray! welcome to our api!' });
});


router.route('/models').get(function(req, res) {
  var types = [];
  for(var k in models) {
    types.push(k);
  }
  res.json(types);
});

router.route('/models/:type').get(function(req, res) {
  var type = req.params.type;
  switch(type) {
    case 'Entry':
      Entry.list(null, 1000).then(function(response){
        if(response.hits && response.hits.hits) {
            res.json( response.hits.hits );
        }
      });
    break;
  }
});

router.route('/models/:type/:id').get(function(req, res) {
  var id = req.params.id;
  var type = req.params.type;
  switch(type) {
    case 'Entry':
      Entry.get(id).then(function(entry){
        entry.listVersions(null,1000).then(function(versions){
          entry.versions = versions;
          res.json(entry);
        });
      });
    break;
  }
});

app.use('/api', router);

app.get('/', function(req, res) {

  res.setHeader('Content-Type', 'text/html');
  res.sendfile(publicDir+'/index.html');
});


  /*
  var a = new Date().getTime();
  var start = moment().toDate();
  var end = moment().add(2,'day').toDate();
  var drivers = driverManager.driverFinder(start,end);
  var b = new Date().getTime() - a;
  res.send(drivers.length+" time:"+b+"ms");
  */
/*
  var entry = new Entry({amount:1});
  entry.save();
*/

//Entry.generate(100, Entry.Currency.EUR, 'super label').save();

/*
  Entry.get('xnh-56RZTbmb0j33RlTeGA').then(function(entry){
    console.log('entry retrieved');

    entry.listVersions(null, 1000).then(function(versions){
      console.log('version retrieved');

      res.setHeader('Content-Type', 'application/json');
      var json = entry.toExt();
      json.versions = versions;
      res.send(JSON.stringify(json, null, 4));

      entry._source.amount++;
      entry.save();

    }, function(error){
      console.log('error in listing vrsions', error);
    });

  });
*/

/*
Entry.list(null, 100).then(function(entries){
  res.setHeader('Content-Type', 'application/json');
  res.send(JSON.stringify(entries, null, 4));
});
*/






var server = app.listen(8080, function(){
  console.log('server is ready on',server.address().port);
});
